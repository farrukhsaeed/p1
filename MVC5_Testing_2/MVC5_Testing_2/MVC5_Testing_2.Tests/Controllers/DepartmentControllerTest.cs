﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MVC5_Testing_2.Tests.Controllers
{
    /// <summary>
    /// Summary description for DepartmentControllerTest
    /// </summary>
    [NUnit.Framework.TestFixture]
    public class DepartmentControllerTest
    {/// <summary>
     /// Test the Action metjod returning Specific Index View
     /// </summary>
        [NUnit.Framework.Test]
        public void TestDepartmentIndex()
        {
            var obj = new MVC5_Testing_2.Controllers.DepartmentController();

            var actResult = obj.Index() as System.Web.Mvc.ViewResult;

            Assert.AreEqual(actResult.ViewName, "Index");
        }


        /// <summary>
        /// Testing the RedirectToRoute to Check for the Redirect
        /// to Index Action
        /// </summary>
        [NUnit.Framework.Test]
        public void TestDepartmentCreateRedirect()
        {
            var obj = new MVC5_Testing_2.Controllers.DepartmentController();

            System.Web.Mvc.RedirectToRouteResult result = obj.Create(new Models.Department()
            {
                DeptNo = 197,
                Dname = "D1",
                Location = "L1"
            }) as System.Web.Mvc.RedirectToRouteResult;


            
            Assert.AreEqual(result.RouteValues["action"], "Index");

        }
    }
}
