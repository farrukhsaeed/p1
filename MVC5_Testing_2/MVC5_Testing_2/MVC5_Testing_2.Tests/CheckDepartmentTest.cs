﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MVC5_Testing_2.ModelAccess;

namespace MVC5_Testing_2.Tests
{
    [NUnit.Framework.TestFixture]
    public class CheckDepartmentTest
    {
        [NUnit.Framework.Test]
        public void CheckDepartmentExist()
        {
            var obj = new ModelAccess.DepartmentAccess();

            var Res = obj.CheckDeptExist(2);
            
            Assert.IsTrue(Res);
        }

        [NUnit.Framework.Test]
        public void CheckDepartmentExistWithMoq()
        {
            //Create Fake Object
            var fakeObject = new Mock<ModelAccess.IDepartmentAccess>();
            //Set the Mock Configuration
            //The CheckDeptExist() method is call is set with the Integer parameter type
            //The Configuration also defines the Return type from the method  
            fakeObject.Setup(x => x.CheckDeptExist(It.IsAny<int>())).Returns(true);
            //Call the methid
            var Res = fakeObject.Object.CheckDeptExist(10);

            Assert.IsTrue(Res);
        }
    }
}
