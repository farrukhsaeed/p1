﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC5_Testing_2.Controllers
{
    public class DepartmentController : Controller
    {
        ModelAccess.DepartmentAccess objds;

        public DepartmentController()
        {
            objds = new ModelAccess.DepartmentAccess();
        }

        // GET: Department
        public ActionResult Index()
        {
            var Depts = objds.GetDepartments();
            return View("Index", Depts);
        }



        // GET: Department/Create
        public ActionResult Create()
        {
            var Dept = new Models.Department();
            return View(Dept);
        }

        // POST: Department/Create
        [HttpPost]
        public ActionResult Create(Models.Department dept)
        {
            try
            {
                objds.CreateDepartment(dept);
                return RedirectToAction("Index");
            }
            catch
            {
                return View(dept);
            }
        }
    }
}