﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC5_Testing_2.ModelAccess
{
    public class DepartmentAccess : IDepartmentAccess
    {
        Models.CompanyEntities ctx;
        public DepartmentAccess()
        {
            ctx = new Models.CompanyEntities();
        }
        /// <summary>
        /// Get All Departments
        /// </summary>
        /// <returns></returns>
        public List<Models.Department> GetDepartments()
        {
            var depts = ctx.Departments.ToList();
            return depts;
        }
        /// <summary>
        /// Get Department base on Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Models.Department GetDepartment(int id)
        {
            var dept = ctx.Departments.Find(id);
            return dept;
        }
        /// <summary>
        /// Create Department
        /// </summary>
        /// <param name="dept"></param>
        public void CreateDepartment(Models.Department dept)
        {
            ctx.Departments.Add(dept);
            ctx.SaveChanges();
        }
        /// <summary>
        /// Check whether Department Exist or Not
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool CheckDeptExist(int id)
        {
            var dept = ctx.Departments.Find(id);
            if (dept != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}