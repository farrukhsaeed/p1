﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVC5_Testing_2.ModelAccess
{
    public interface IDepartmentAccess
    {
        List<Models.Department> GetDepartments();
        Models.Department GetDepartment(int id);
        void CreateDepartment(Models.Department dept);
        bool CheckDeptExist(int id);
    }
}
